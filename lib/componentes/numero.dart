import 'package:arabic_numbers/arabic_numbers.dart';
import 'package:bloc_practice2/bloc_contador/bloc_contador.dart';
import 'package:bloc_practice2/bloc_contador/estados.dart';
import 'package:flutter/material.dart';
import 'package:numerus/numerus.dart';
import 'package:provider/src/provider.dart';

class VistaNumero extends StatelessWidget {
  const VistaNumero(this.numero, this.opcionNumeral, { Key? key }) : super(key: key);
  final int numero;
  final int opcionNumeral;

  @override
  Widget build(BuildContext context) {
    final estado = context.watch<BlocContador>().state;
    final int numero = (estado as Numero).numero;

    String texto = numero.toString();
    ArabicNumbers arabicNumber = ArabicNumbers();

    if(opcionNumeral == 1) {
      texto = numero.toRomanNumeralString()!;
    }
    if(opcionNumeral == 2) {
      texto = arabicNumber.convert(numero);
    }


    return Center(
      child: Text(texto, style: Theme.of(context).textTheme.headline1,)
    );
  }
}