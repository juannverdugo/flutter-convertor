import 'package:fpdart/fpdart.dart';
import 'package:http/http.dart' as http;

import 'package:bloc_practice2/dominios/interfase_contador.dart';

class RepositorioRemoto extends InterfaseRepositorios {
  @override
  Future<Either <String, int>> obtenerNumero({required int minimo, required int maximo}) async {
    // final String peticion = 'https://www.random.org/integers/?num=1&min=$minimo&max=$maximo&col=1&base=10&format=plain&rnd=new';
    final String peticion = '';
    int? entero;

    try {
      Uri peticionUrl = Uri.parse(peticion);
      http.Response respuesta = await http.get(peticionUrl);

      String respuestaFinal = respuesta.body;
      // String respuestaCruda = respuesta.body;
      // String respuestaProcesada = respuestaCruda.substring(1, respuestaCruda.length - 1);

      entero = int.parse(respuestaFinal);
    } catch (e) {
      return const Left('Problema en acceso a repositorio remoto');
    }
    
    return Right(entero);
    
  }

}