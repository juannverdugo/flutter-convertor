import 'dart:math';

import 'package:bloc_practice2/dominios/interfase_contador.dart';
import 'package:fpdart/fpdart.dart';

class RepositorioLocal extends InterfaseRepositorios {
  @override
  Future<Either <String, int>> obtenerNumero({required int minimo, required int maximo}) {
    assert(maximo >= minimo, 'El maximo debe ser mayor o igual al número');
    int valor = Random().nextInt(maximo - minimo +1)+minimo;
    return Future.value(Right(valor));
  }
  
}