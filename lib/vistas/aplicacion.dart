import 'dart:html';

import 'package:bloc_practice2/bloc_contador/bloc_contador.dart';
import 'package:bloc_practice2/bloc_contador/estados.dart';
import 'package:bloc_practice2/bloc_contador/eventos.dart';
import 'package:bloc_practice2/vistas/principal.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Aplicacion extends StatelessWidget {
  const Aplicacion({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Bloc Convert'),
          actions: [const Opciones()]),
        body: const VistaPrincipal(),
        floatingActionButton: const Botones(),
        persistentFooterButtons: [const Deportes()],
      )
    );
  }
}

class Botones extends StatelessWidget {
  const Botones({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final estado = context.watch<BlocContador>().state;

    if((estado is Numero) || (estado is Imagen)) {
      return Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 0, 30),
              child: FloatingActionButton(
                onPressed: (){
                  context.read<BlocContador>().add(Modificado(1));
                },
                child: const Icon(Icons.add)
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(60, 0, 0, 30),
              child: FloatingActionButton(
                onPressed: (){
                  context.read<BlocContador>().add(Modificado(-1));
                },
                child: const Icon(Icons.remove)
              ),
            )
          ],
        );
    }
    return Container();
  }
}


class Opciones extends StatefulWidget {
  const Opciones({ Key? key }) : super(key: key);

  @override
  State<Opciones> createState() => _OpcionesState();
}

class _OpcionesState extends State<Opciones> {
  final List<String> titulos = [
    'Occidente', 
    'Romanos', 
    'Arábigos'];

  late int opcionSeleccionado;

  @override
  void initState() {
    super.initState();
    opcionSeleccionado = 0;
  }

  @override
  Widget build(BuildContext context) {
    final estado = context.watch<BlocContador>().state;

    if(estado is Problema) return Container();
    return Wrap(
      children: [
        ...titulos
          .asMap()
          .entries
          .map((e) => Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ChoiceChip(
                  label: Text(e.value), 
                  labelStyle: const TextStyle(color: Colors.white),
                  shape: const StadiumBorder(side: BorderSide(color: Colors.blue)),
                  avatar: CircleAvatar(backgroundColor: Colors.white, child: Text((e.key + 1).toString()),),
                  selected: opcionSeleccionado == e.key,
                  onSelected: (seleccionado) {
                    setState(() {
                      context.read<BlocContador>().add(ModificadoNumerales(e.key));
                      opcionSeleccionado = e.key;
                    });
                  },
                  backgroundColor: Colors.blue,
                  selectedColor: Colors.blueGrey,
                  selectedShadowColor: Colors.deepOrange,
                ),
              )
            )
          ).toList()
      ],
    );
  }
}

enum SingingCharacter { futbol, basketball }

class Deportes extends StatefulWidget {
  const Deportes({ Key? key }) : super(key: key);

  @override
  State<Deportes> createState() => _DeportesState();
}

class _DeportesState extends State<Deportes> {
  SingingCharacter? _character = SingingCharacter.futbol;

  @override
  Widget build(BuildContext context) {
    final estado = context.watch<BlocContador>().state;

    if(estado is Problema) return Container();
    return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
              ListTile(
                title: const Text('Futbol'),
                leading: Radio<SingingCharacter>(
                  value: SingingCharacter.futbol,
                  groupValue: _character,
                  onChanged: (SingingCharacter? value) {
                    setState(() { _character = value; });
                  },
                ),
              ),
              ListTile(
                title: const Text('Basketball'),
                leading: Radio<SingingCharacter>(
                  value: SingingCharacter.basketball,
                  groupValue: _character,
                  onChanged: (SingingCharacter? value) {
                    setState(() { _character = value; });
                  },
                ),
              ),
          ],
        ),
      );
  }
}

