import 'package:bloc_practice2/repositorios/local.dart';
import 'package:bloc_practice2/repositorios/remoto.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:bloc_practice2/vistas/aplicacion.dart';
import 'package:bloc_practice2/bloc_contador/bloc_contador.dart';

void main() {
  runApp(const Proveedor());
}

class Proveedor extends StatelessWidget {
  const Proveedor({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider(
          create: (context) => RepositorioRemoto(),
        ),
        RepositoryProvider(
          create: (context) => RepositorioLocal(),
        ),
      ],
      child: BlocProvider(
        create: (context) => BlocContador(context.read<RepositorioRemoto>()),
        child: const Aplicacion(),
      ),
    );
  }
}
